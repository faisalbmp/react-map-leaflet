<h1>Untuk backend</h1>

pada terminal:

<ul>
<li>
  cd ./backend-map
</li>
<li>
  npm install
</li>
<li>
  node app.js
</li>
</ul>

<h1>Untuk frontend</h1>

pada terminal:

<ul>
<li>
  cd ./frontend-map
</li>
<li>
  npm install
</li>
<li>
  npm start
</li>
</ul>
