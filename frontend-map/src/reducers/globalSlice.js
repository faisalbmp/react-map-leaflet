import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice
} from '@reduxjs/toolkit';
import { getMapData } from './globalApi';

// const globalAdapter = createEntityAdapter();


export const fetchMapData = createAsyncThunk(
  'global/get',
  async () => {
    try {
      const response = await getMapData();
    return response.data
    }catch  (err){
      throw err;
    }
  }
)
const initialState = {
  loading: 'idle',
  currentRequestId: undefined,
  error: null,
  maps:[]
}

export const globalSlice = createSlice({
  name: 'global',
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [fetchMapData.pending]: (state, action) => {
      state.loading = 'pending'
      state.currentRequestId = action.meta.requestId
    },
    [fetchMapData.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle';
        state.currentRequestId = undefined;
        state.error = null;
        state.maps = action.payload;
      }
    },
    [fetchMapData.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.payload && action.payload.message ? action.payload.message : action.message
        state.currentRequestId = undefined
      }
    }
  }
});

const { reducer } = globalSlice;
export default reducer;

/* export const {
  selectById: selectGlobalById,
  selectIds: selectGlobalIds,
  selectEntities: selectGlobalEntities,
  selectAll: selectAllGlobal,
  selectTotal: selectTotalGlobal
} = globalAdapter.getSelectors((state) => state.global); */