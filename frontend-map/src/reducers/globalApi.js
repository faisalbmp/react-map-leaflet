import api from "../services/api";

export const getMapData = () => {
  return api.get();
};