import { combineReducers } from 'redux';

import { default as globalReducer } from './globalSlice';


export default combineReducers({
  global: globalReducer,
});
