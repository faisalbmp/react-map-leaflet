import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import MapContainer from "../components/Map/MapContainer";
import { fetchMapData } from "../reducers/globalSlice";
import { Card, CardBody, CardText, Container } from "reactstrap";
const Dashboard = () => {
  const dispatch = useDispatch();

  const [userPosition, setuserPosition] = useState({
    lat: -6.899373712973409,
    lng: 107.61596560478212
  })
  const [positions, setpositions] = useState([])
  const [haveUserLocation, sethaveUserLocation] = useState(false)
  const [zoom, setzoom] = useState(2)
  useEffect(() => {
    dispatch(
      fetchMapData()
    ).then(res => {

      if (fetchMapData.fulfilled.match(res)) {
        setpositions(res.payload?.map(({ LATITUDE, LONGITUDE, sitename }) => {
          return {
            lat: LATITUDE,
            lng: LONGITUDE,
            label: sitename,
          }
        }))
      }
    });
    navigator.geolocation.getCurrentPosition(position => {
      setuserPosition({
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      })
      sethaveUserLocation(true)
      setzoom(13)
    }, () => {
      console.log(`fetch location using ip Address`);
      const url = "https://ipapi.co/json"
      fetch(url)
        .then(res => res.json())
        .then(location => {
          setuserPosition({
            lat: location.latitude,
            lng: location.longitude,
          })
          sethaveUserLocation(true)
          setzoom(13)
        })
    }, { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 })
    /* setpositions(allData?.map(({ LATITUDE, LONGITUDE, sitename }) => {
      return {
        lat: LATITUDE,
        lng: LONGITUDE,
        label: sitename,
      }
    })) */
  }, [dispatch])

  return (
    <MapContainer
      userPosition={userPosition}
      zoom={zoom}
      data={positions}
      haveUserLocation={haveUserLocation}
    />
  )
}

export default Dashboard;