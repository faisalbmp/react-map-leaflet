import React from 'react';
import L from "leaflet";
import machineIcon from '../../assets/img/iconTransaction.png'
import userIcon from '../../assets/img/user.png'
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
const stamenTonerTiles = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
const stamenTonerAttr = 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';

var deviceMark = L.icon({
  iconUrl: machineIcon,
  iconSize: [25, 41],
  // iconAnchor: [22, 94],
  popupAnchor: [0, -26]
});

var userMark = L.icon({
  iconUrl: userIcon,
  iconSize: [25, 41],
  // iconAnchor: [22, 94],
  popupAnchor: [0, -26]
});

export default function MapContainer({ input, zoom, latlng, data, userPosition, haveUserLocation }) {
  return (
    <Map
      {...input}
      value={latlng}
      center={userPosition}
      zoom={zoom}
      style={
        {
          height: "1000px",
          width: "100%",
        }
      }
    >
      <TileLayer
        attribution={stamenTonerAttr}
        url={stamenTonerTiles}
      />
      {haveUserLocation ?
        <Marker icon={userMark} position={userPosition}></Marker>
        : ''}
      {data.map(machine => {
        const { lat, lng, label } = machine;
        return (
          <Marker icon={deviceMark} position={{
            lat,
            lng
          }}>
            <Popup >
              {label}
            </Popup>
          </Marker>
        )
      })}
    </Map>
  );
}

