const express = require('express');
const app = express();
var cors = require('cors')
const data = require("./exreturnMarker.json")

app.use(cors())
//route untuk halaman home
app.get('/',(req, res) => {
  res.json(data);
});
 
//route untuk halaman about
app.get('/about',(req, res) => {
  res.send('This is about page');
});
 
app.listen(8000, () => {
  console.log('Server is running at port 8000');
});